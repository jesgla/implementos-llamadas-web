import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ArticulosService {

  /**
   *Creates an instance of ArticulosService.
   * @param {HttpClient} httpClient
   * @memberof ArticulosService
   */
  constructor(private httpClient: HttpClient) { }
  /**
   * header para ejecutar consultas post
   * @memberof ArticulosService
   */
  header = new HttpHeaders(
    {
      "Accept": 'application/json',
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin':'*',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'
    });

  /**
   *Permite obtener listado de articulos
   * @param {*} parametros
   * @returns
   * @memberof ArticulosService
   */
  async obtenerArticulos(parametros) {
    let consulta = null;

    const url = `https://b2b-api.implementos.cl/api/movil/busquedaProductos`;
    //const url =  `${environment.endPointB2B}${environment.Articulos.methods.obtenerArticulos}`;


    consulta = await this.httpClient.post(url, parametros, { headers: this.header }).toPromise();
 
    return consulta;
  }
}
