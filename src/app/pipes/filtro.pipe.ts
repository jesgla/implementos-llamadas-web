import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filtro'
})
export class FiltroPipe implements PipeTransform {

  transform(arreglo: any[],
    texto: string,
    columna: string,
    columna2: string,
    columna3: string,
    columna4: string
  ): any[] {

    if (texto.trim().length === 0) {
      return arreglo;
    }

    texto = texto.toLowerCase();


    return arreglo.filter(item => {
      return item[columna].toLowerCase()
        .includes(texto) 
        ||
        item[columna2].includes(texto)
        ||
        item[columna3].toLowerCase()
          .includes(texto)
        ||
        item[columna4].toLowerCase()
          .includes(texto)
    });

  }
}
