import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FiltroPipe } from './filtro.pipe';
import { FormatoModedaPipe } from './formato-modeda.pipe';



@NgModule({
  declarations: [
    FiltroPipe,
    FormatoModedaPipe,
  ],
  imports: [
    CommonModule
  ],
  exports:[
    FiltroPipe,
    FormatoModedaPipe]
})
export class PipesModule { }
